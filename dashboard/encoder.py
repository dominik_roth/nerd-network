from django.core.serializers.json import DjangoJSONEncoder
from dashboard.models import UserPost

class PostEncoder(DjangoJSONEncoder):
    def default(self, o):
        return str(o)