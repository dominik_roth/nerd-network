from django.core.management import BaseCommand
from dashboard.utils import TestUtilities


class Command(BaseCommand):
    help = "Creates Test Posts"

    def handle(self, *args, **kwargs):
        TestUtilities.generate_post_test_data()
