from django.contrib.auth.models import AbstractUser
from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from uuid import uuid4

# Create your models here.
from nerd_network import settings


class CustomUser(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)
    profile_icon = models.ImageField(upload_to='profile_icons/' + str(uuid4()) + '/', blank=True, null=True)

    def __str__(self):
        return self.username + ":" + self.email

    def get_icon(self):
        try:
            icon = self.profile_icon.url
        except ValueError:
            from django.templatetags import static
            icon = static.static('img/no_profile_img_placeholder.png')
        return icon


@receiver(post_save, sender=get_user_model())
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        from rest_framework.authtoken.models import Token
        Token.objects.create(user=instance)


class UserFollowingWrapper(models.Model):
    account = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='account_owner')
    following = models.ManyToManyField(get_user_model())

    def __str__(self):
        return str(self.account)


class Group(models.Model):
    name = models.CharField(max_length=64)
    description = models.TextField(blank=True, null=True)
    leader = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='group_leader')
    members = models.ManyToManyField(get_user_model(), related_name='group_members')
    group_icon = models.ImageField(upload_to='group_icons/' + str(uuid4()) + '/', blank=True, null=True)

    def __str__(self):
        return self.name


class UserPost(models.Model):
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='post_author')
    text = models.TextField(max_length=256)
    posted_date = models.DateTimeField(default=timezone.now)
    data = models.FileField(blank=True, null=True)

    def __str__(self):
        return self.text[:32]

    def as_dict(self):
        return {
            'id': self.id,
            'author': str(self.author),
            'posted_date': self.posted_date,
            'text': self.text,
        }


class GroupPost(models.Model):
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='group_post_author')
    text = models.TextField(max_length=512)
    posted_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.text[:32]

    def as_dict(self):
        return {
            'id': self.id,
            'author': str(self.author),
            'posted_date': self.posted_date,
            'text': self.text,
        }



