from django.urls import path
from . import views
from .forms import UserPostForm


urlpatterns = [

    # REST API URLs
    #path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
    path('api/user-timeline', views.GetUserTimeline.as_view(), name='api-user-timeline'),
    path('api/submit-user-post-form', views.SubmitPostAPI.as_view(), name='api-submit-user-post'),
]