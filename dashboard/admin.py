from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(CustomUser)
admin.site.register(Group)
admin.site.register(UserPost)
admin.site.register(GroupPost)
admin.site.register(UserFollowingWrapper)
