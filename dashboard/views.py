from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from .forms import UserPostForm
from .utils import ViewUtilities
from rest_framework.response import Response as restResponse

vu = ViewUtilities()


# REST API Views
class GetUserTimeline(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        if not 'start' in request.GET:
            return restResponse('Start not provided', status=400)
        if not 'limit' in request.GET:
            limit = 20
        else:
            limit = int(request.GET['limit'])
        start = int(request.GET['start'])
        data = vu.get_user_timeline(request)
        return restResponse(data[start:start+limit])


class SubmitPostAPI(APIView):
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        form = UserPostForm(request.POST)
        if form.is_valid():
            model = form.save(commit=False)
            model.author = request.user
            model.save()
            return restResponse(status=201)
        return restResponse(status=400)
